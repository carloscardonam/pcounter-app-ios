//
//  Item.swift
//  pCounter
//
//  Created by Carlos Cardona on 22/01/21.
//

import Foundation
import RealmSwift

class Item: Object {
    @objc dynamic var date: String = ""
    @objc dynamic var totalPeople: Int = 0
    @objc dynamic var women: Int = 0
    @objc dynamic var men: Int = 0
}
