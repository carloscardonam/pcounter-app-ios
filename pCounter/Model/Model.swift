//
//  Model.swift
//  pCounter
//
//  Created by Carlos Cardona on 20/01/21.
//

import UIKit
import RealmSwift

class Model: Object{
    
    static var shared = Model()
    
    @objc dynamic public var counter = 0
    @objc dynamic public var menCounter = 0
    @objc dynamic public var womenCounter = 0
    @objc dynamic public var maxValue = 15
    @objc dynamic public var createdDate = "12-12-12 12:12"
    
    public var itemsArray = [Model]()
    
    public func setDate() -> String {
        
        let date = Date()
        
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.locale = .current
        formatter.dateFormat = "MM-dd-yyyy HH:mm"
        
        createdDate = formatter.string(from: date)
        
        return createdDate
    }
    
    public func setWomenMax(vc: UIViewController ,completion: @escaping() -> ()) {
        if counter < maxValue {
            counter += 1
            womenCounter += 1
        } else {
            let alert = UIAlertController(title: "Maximum capacity reached", message: "Please wait until a client leave", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            alert.addAction(UIAlertAction(title: "Edit Max. People", style: .destructive, handler: { (action) in
                completion()
            }))
            vc.present(alert, animated: true)
        }
    }
    
    public func setMenMax(vc: UIViewController, completion: @escaping() -> ()) {
        if counter < maxValue {
            counter += 1
            menCounter += 1
        } else {
            let alert = UIAlertController(title: "Maximum capacity reached", message: "Please wait until a client leave", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            alert.addAction(UIAlertAction(title: "Edit Max. People", style: .destructive, handler: { (action) in
                completion()
            }))
            vc.present(alert, animated: true)
        }
    }
    
}
