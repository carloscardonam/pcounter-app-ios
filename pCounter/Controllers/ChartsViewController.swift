//
//  ChartsViewController.swift
//  pCounter
//
//  Created by Carlos Cardona on 20/01/21.
//

import UIKit
import RealmSwift

class ChartsViewController: UIViewController {
    
    let realm = try! Realm()
    
    var itemsArray: Results<Model>?
    
    private var tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "dayCell")
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadItems()
        view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadItems()
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = CGRect(x: 0, y: 0, width: view.width, height: view.heigth - view.safeAreaInsets.top)
    }
    
    
    // MARK: - Data Manipuation Methods
    
    func loadItems() {
        itemsArray = realm.objects(Model.self)
    }
}

extension ChartsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        itemsArray?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "dayCell", for: indexPath)
        
        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "dayCell")
        
        if let item = itemsArray?[indexPath.row] {
            cell.textLabel?.text = item.createdDate
            cell.detailTextLabel?.text = "Total: \(item.counter), Women: \(item.womenCounter), Men: \(item.menCounter)"
            cell.detailTextLabel?.numberOfLines = 0
        } else {
            cell.textLabel?.text = "No days counters registered"
        }
        
        cell.accessoryType = .disclosureIndicator
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
