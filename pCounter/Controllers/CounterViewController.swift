//
//  CounterViewController.swift
//  pCounter
//
//  Created by Carlos Cardona on 19/01/21.
//

import UIKit
import RealmSwift

class CounterViewController: UIViewController {
    
    let realm = try! Realm()
    var items: Results<Model>?
    
    var counter = 0
    var womenCounter = 0
    var menCounter = 0
    
    var date = Model.shared.setDate()
    
    private var counterLabel: UILabel = {
        let label = UILabel()
        label.text = "0"
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 90)
//        label.backgroundColor = .blue
        return label
    }()
    
    private var counterTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "People"
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 17)
//        label.backgroundColor = .blue
        return label
    }()
    
    private var womenImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "Women")
//        image.backgroundColor = .blue
        image.contentMode = .scaleAspectFit
        image.layer.masksToBounds = true
        image.layer.cornerRadius = CGFloat(10)
        image.alpha = 0.9
        return image
    }()
    
    private var menImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "Men")
        image.contentMode = .scaleAspectFit
        image.layer.masksToBounds = true
        image.layer.cornerRadius = CGFloat(10)
        image.alpha = 0.9
        return image
    }()
    
    private var womenPlusButton: UIButton = {
        let button = UIButton()
//        button.backgroundColor = .yellow
        button.alpha = 0.3
        return button
    }()
    
    private var womenMinusButton: UIButton = {
        let button = UIButton()
//        button.backgroundColor = .blue
        button.alpha = 0.3
        return button
    }()
    
    private var menPlusButton: UIButton = {
        let button = UIButton()
//        button.backgroundColor = .yellow
        button.alpha = 0.3
        return button
    }()
    
    private var menMinusButton: UIButton = {
        let button = UIButton()
//        button.backgroundColor = .blue
        button.alpha = 0.3
        return button
    }()
    
    private var maxPeopleLabel: UILabel = {
        let label = UILabel()
        label.text = "The default max number of people allowed is of: 15, tap on the \("Edit") button to change it"
        label.font = label.font.withSize(10)
        label.numberOfLines = 0
        label.textColor = .gray
        return label
    }()
    
    private var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save", for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 18)
        button.setTitleColor(#colorLiteral(red: 0.8974260688, green: 0.5800501704, blue: 0.3831537366, alpha: 1), for: .normal)
        return button
    }()
    
    
//    private var configureBarButtonItem: UIBarButtonItem = {
//        let button = UIBarButtonItem()
//        button.image = UIImage(systemName: "slider.horizontal.3")
//        button.tintColor = #colorLiteral(red: 0.8974260688, green: 0.5800501704, blue: 0.3831537366, alpha: 1)
//        button.style = .plain
//        button.action = #selector(configureBarButtonPressed(_:))
//        return button
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        
        
        womenPlusButton.addTarget(self, action: #selector(womenPlusButtonPressed(_:)), for: .touchUpInside)
        womenMinusButton.addTarget(self, action: #selector(womenMinusButtonPressed(_:)), for: .touchUpInside)
        
        menPlusButton.addTarget(self, action: #selector(menPlusButtonPressed(_:)), for: .touchUpInside)
        menMinusButton.addTarget(self, action: #selector(menMinusButtonPressed(_:)), for: .touchUpInside)
        
        saveButton.addTarget(self, action: #selector(saveButtonPressed(_:)), for: .touchUpInside)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(configureBarButtonPressed(_:)))
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // Counter
        counterLabel.frame = CGRect(x: 20, y: view.safeAreaInsets.top + 40, width: view.width - 40, height: 70)
        counterTitleLabel.frame = CGRect(x: 20, y: counterLabel.bottom + 5, width: view.width - 40, height: 25)
        // ImageViews
        womenImageView.frame = CGRect(x: 10, y: counterTitleLabel.bottom + 15, width: view.width - 20, height: 110)
        menImageView.frame = CGRect(x: 10, y: womenImageView.bottom + 5, width: view.width - 20, height: 110)
        // Women
        womenPlusButton.frame = CGRect(x: womenImageView.left, y: womenImageView.top, width: womenImageView.width / 2, height: womenImageView.heigth)
        womenMinusButton.frame = CGRect(x: womenPlusButton.right, y: womenImageView.top, width: womenImageView.width / 2, height: womenImageView.heigth)
        // Men
        menPlusButton.frame = CGRect(x: menImageView.left, y: menImageView.top, width: menImageView.width / 2, height: menImageView.heigth)
        menMinusButton.frame = CGRect(x: menPlusButton.right, y: menImageView.top, width: menImageView.width / 2, height: menImageView.heigth)
        
        maxPeopleLabel.frame = CGRect(x: 10, y: menMinusButton.bottom + 10, width: view.width - 20, height: 30)
        
        saveButton.frame = CGRect(x: 10, y: maxPeopleLabel.bottom + 10, width: view.width - 20, height: 50)
        
    }
    
    func addSubviews() {
        view.addSubview(counterLabel)
        view.addSubview(counterTitleLabel)
        view.addSubview(maxPeopleLabel)
        
        view.addSubview(womenImageView)
        view.addSubview(menImageView)
        
        view.addSubview(womenPlusButton)
        view.addSubview(womenMinusButton)
        
        view.addSubview(menPlusButton)
        view.addSubview(menMinusButton)
        
        view.addSubview(saveButton)
    }
    
    func setDateAtBegining() -> String{
        if Model.shared.counter == 0 {
            let date = Date()

            let formatter = DateFormatter()
            formatter.timeZone = .current
            formatter.locale = .current
            formatter.dateFormat = "MM-dd-yyyy HH:mm"
            
            Model.shared.createdDate = formatter.string(from: date)

            print(formatter.string(from: date))
            
            
        }
        
        return Model.shared.createdDate
    }
    
    // MARK: - Functiionality
    
    @objc private func womenPlusButtonPressed(_ sender: UIButton) {
        
        Model.shared.setWomenMax(vc: self) {
            let vc = ConfigureViewController()
            let navVc = UINavigationController(rootViewController: vc)
            navVc.navigationBar.prefersLargeTitles = true
            self.present(navVc, animated: true)
        }
        
        counterLabel.text = String(Model.shared.counter)
        print("COUNTER")
        print(Model.shared.counter)
        print("WOMEN COUNTER")
        print(Model.shared.womenCounter)
        print("MEN COUNTER")
        print(Model.shared.menCounter)
        print("DATE")
        print(Model.shared.createdDate)
        
    }
    
    @objc private func womenMinusButtonPressed(_ sender: UIButton) {
        
        Model.shared.counter -= 1
        Model.shared.womenCounter -= 1
        
        counterLabel.text = String(Model.shared.counter)
        print(Model.shared.counter)
    }
    
    @objc private func menPlusButtonPressed(_ sender: UIButton) {
        
        Model.shared.setMenMax(vc: self) {
            let vc = ConfigureViewController()
            let navVc = UINavigationController(rootViewController: vc)
            navVc.navigationBar.prefersLargeTitles = true
            self.present(navVc, animated: true)
        }
        
        counterLabel.text = String(Model.shared.counter)
    }
    
    @objc private func menMinusButtonPressed(_ sender: UIButton) {
       
        Model.shared.counter -= 1
        Model.shared.menCounter -= 1
        
        counterLabel.text = String(Model.shared.counter)
        print(Model.shared.counter)
    }
    
    @objc private func configureBarButtonPressed(_ sender: UIBarButtonItem) {
        print("Tapped")
        let vc = ConfigureViewController()
        let navVc = UINavigationController(rootViewController: vc)
        navVc.navigationBar.prefersLargeTitles = true
        present(navVc, animated: true)
    }
    
    @objc private func saveButtonPressed(_ sender: UIButton) {
        
        if Model.shared.counter != 0 {
            let alert = UIAlertController(title: "Are you sure you want to save?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Sure", style: .default, handler: { (actiom) in
                
                let newItem = Model()
                
                newItem.counter = Model.shared.counter
                newItem.menCounter = Model.shared.menCounter
                newItem.womenCounter = Model.shared.womenCounter
                newItem.createdDate = Model.shared.setDate()
                Model.shared.itemsArray.append(newItem)
                self.save(item: newItem)
                
                print(Model.shared.itemsArray.count)
                Model.shared.counter = 0
                Model.shared.menCounter = 0
                Model.shared.womenCounter = 0
                
                self.counterLabel.text = "0"
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "There is no people registered", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default))
            present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    
    // MARK: - Data Manipulation Methods
    
    func save(item: Model) {
        
        do {
            try realm.write {
                realm.add(item)
            }
        } catch {
            print("Error trying save Item::: \(error)")
        }
        
    }
    
    
}
