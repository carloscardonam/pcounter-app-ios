//
//  ConfigureViewController.swift
//  pCounter
//
//  Created by Carlos Cardona on 19/01/21.
//

import UIKit

class ConfigureViewController: UIViewController {
    
    private var maxPeopleLabel: UILabel = {
        let label = UILabel()
        label.text = "Max People"
        label.textAlignment = .left
        return label
    }()
    
    private var maxTextField: UITextField = {
        let field = UITextField()
        field.placeholder = "Max."
        field.backgroundColor = .secondarySystemBackground
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        field.leftViewMode = .always
        field.keyboardType = .numberPad
        field.layer.masksToBounds = true
        field.layer.cornerRadius = CGFloat(10)
        field.becomeFirstResponder()
        return field
    }()
    
    private var saveButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 0.8974260688, green: 0.5800501704, blue: 0.3831537366, alpha: 0.6211981741)
        button.setTitle("Save", for: .normal)
        button.layer.cornerRadius = CGFloat(10)
        button.layer.masksToBounds = true
        return button
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Edit"
        view.backgroundColor = .systemBackground
        
        addSUbviews()
        
        saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        maxPeopleLabel.frame = CGRect(x: 20, y: view.safeAreaInsets.top + 20, width: (view.width / 2) - 20, height: 40)
        maxTextField.frame = CGRect(x: maxPeopleLabel.right + 10, y: view.safeAreaInsets.top + 20, width: (view.width / 2) - 30, height: 40)
        
        saveButton.frame = CGRect(x: view.width / 2 - 50, y: maxPeopleLabel.bottom + 70, width: 100, height: 50)
    }
    
    func addSUbviews() {
        view.addSubview(maxTextField)
        view.addSubview(maxPeopleLabel)
        view.addSubview(saveButton)
        
    }
    
    @objc func saveButtonTapped() {
        if let text = maxTextField.text {
            Model.shared.maxValue = Int(text)!
        }
        
        print(Model.shared.maxValue)
        print("tapped save button")
        self.dismiss(animated: true)
    }
}
